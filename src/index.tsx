import { render } from 'react-dom';

import { defaultTheme } from '@src/types/theme';

import App from './App';

render(<App theme={defaultTheme} />, document.getElementById('root'));
