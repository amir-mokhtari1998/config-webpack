import React, { useEffect, useState, memo } from 'react';
import styled from 'styled-components';

import Message from './Message';
import Image from '@assets/image/bot.png';
import { UseChatScroll } from '@src/helper/scrollChat';
import { delay } from '@src/helper/delay';
import { ScenarioProps } from '@src/types/component/chat-bot';

interface MessageProps {
  title: string;
  id: number;
  type?: string;
}

interface ComponentProps {
  scenario: ScenarioProps;
  setOpenModal?: React.Dispatch<React.SetStateAction<boolean>>;
  heightDiv?: number;
  isOpen?: boolean;
}

const AllMessage: React.FC<ComponentProps> = (props) => {

  const [message, setMessage] = useState<MessageProps[] | []>([]);
  const ref = UseChatScroll(message, props.heightDiv);

  useEffect(() => {
    if (props.scenario?.title && props.scenario?.id || props.scenario?.type === 'SIMPLE') {
      delay(1000)
        .then(() => {
          setMessage(prevState => [...prevState, {
            title: props.scenario?.title,
            id: props.scenario?.id,
            type: props.scenario?.type
          }]);
        })
        .then(() => {
          if (!props.scenario?.responses) {
            delay(800).then(() => props.setOpenModal(true));
          }
          if (props.scenario?.responses) {
            delay(2000).then(() => {
              const delaySendMessage = (items: { [key: string]: any }, interval: number) => {
                if (items.length === 0) {
                  delay(800).then(() => props.setOpenModal(true));
                  return;
                }
                setMessage(prevState => [...prevState, items[0]]);
                setTimeout(() => delaySendMessage(items.slice(1), interval), interval);
              };
              delaySendMessage(props?.scenario?.responses, 2000);
            });
          }
        });
    }
  }, [props.scenario]);

  return (
    <AllMessageStyled heightDiv={props.heightDiv} isOpen={props.isOpen}>
      {
        message && message.map((item) =>
          <Message type={(item.type === 'SIMPLE' || item.type === 'OVER') ? 'user' : 'bot'}
                   message={item.title} image={Image} key={item.title} />
        )
      }
      <div ref={ref} />
    </AllMessageStyled>
  );
};

export default memo(AllMessage);

const AllMessageStyled = styled.div<{ heightDiv?: number, isOpen?: boolean }>`
  padding: 12px;
  overflow-y: scroll;
  ${(props) => (props.isOpen ? `max-height: calc(100% - ${props.heightDiv}px)` : 'max-height: 100%;')};

  ::-webkit-scrollbar {
    width: 0;
  }
`;
