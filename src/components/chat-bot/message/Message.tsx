import React from 'react';
import styled from 'styled-components';

import { Typography, FlexBox } from '@src/theme/index';

interface MyComponentProps {
  type?: string,
  image?: string,
  message?: string
}

const Message: React.FC<MyComponentProps> = (props) => {
  return (
    <MessageWrapperStyled>
      <MessageStyled>
        <FlexBox justify={props.type === 'bot' ? 'start' : 'end'} alignItems='center'>
          {props.type === 'bot' && <MessageImage src={props.image} />}
          <Typography padding='12px 16px' inline size='sm' align='justify' maxWidth='85%' direction='rtl'
                      className={`${props.type === 'bot' ? 'message-bot' : 'message-user'}`}>
            {props.message}
          </Typography>
        </FlexBox>
      </MessageStyled>
    </MessageWrapperStyled>
  );
};

export default Message;

const MessageWrapperStyled = styled.div`
  .message-bot {
    background-color: ${props => props.theme.colors.secondary};
    color: ${props => props.theme.colors.text};
    border-radius: 16px 16px 16px 0;
  }

  .message-user {
    background-color: ${props => props.theme.colors.primary};
    color: ${props => props.theme.colors.secondary};
    border-radius: 16px 16px 0 16px;
  }
`;

const MessageStyled = styled.div`
  width: 100%;
  margin: 10px 0;
`;

const MessageImage = styled.img`
  width: 24px;
  margin-right: 8px;
`;
