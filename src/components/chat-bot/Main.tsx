import React, { useEffect, useState } from 'react';
import styled from 'styled-components';

import HeaderChat from './header/HeaderChat';
import AllMessage from './message/AllMessage';
import OptionLists from './option-lists/OptionLists';
import { FlexBox } from '@src/theme/index';
import { flow } from '../../flow-chat';
import { ScenarioProps } from '@src/types/component/chat-bot';

const Main: React.FC = () => {

  const [openModal, setOpenModal] = useState<boolean>(false);
  const [scenarios, setScenarios] = useState<ScenarioProps>();
  const [overType, isOverType] = useState<boolean>(false);
  const [height, onChangeHeight] = useState<number>(0);

  useEffect(() => {
    if (flow.data.scenario) {
      setScenarios(flow.data.scenario.main_scenario);
    }
  }, []);

  useEffect(() => {
    if (overType) {
      setScenarios(flow.data.scenario.shared_scenarios?.OVER);
    }
  }, [overType]);

  const getList = (item: ScenarioProps) => {
    setOpenModal(false);
    if (item.type === 'OVER') {
      setScenarios(item);
      isOverType(true);
    } else if (item.type === 'SIMPLE') {
      setScenarios(item);
    }
  };

  return (
    <MainStyled alignItems='center' justify='center'>
      <WrapperChatStyled>
        <WrapperChatInnerStyled>
          <HeaderChat />
          <ContentStyled justify='space-between' direction='column'>
            <AllMessage scenario={scenarios} setOpenModal={setOpenModal} heightDiv={height} isOpen={openModal}/>
            <OptionLists isOpen={openModal} textBtn='send' options={scenarios?.options}
                         handleClickButton={getList} onChangeHeight={onChangeHeight}/>
          </ContentStyled>
        </WrapperChatInnerStyled>
      </WrapperChatStyled>
    </MainStyled>
  );
};

export default Main;

const MainStyled = styled(FlexBox)`
  position: relative;
  width: 100%;
  max-height: 100%;
  background-color: ${props => props.theme.colors.secondary};
  height: 100vh;
`;

const WrapperChatStyled = styled.div`
  border-radius: ${props => props.theme.borderRadius.md};
  box-shadow: 0 0px 3px 0px #e4e4e4;
  width: 350px;
  height: 95%;
  background-color: ${props => props.theme.colors.secondary};
  padding: 8px;
  overflow: hidden;
`;

const WrapperChatInnerStyled = styled.div`
  background-color: #F7F9FC;
  height: 100%;
  width: 100%;
`;

const ContentStyled = styled(FlexBox)`
  height: calc(100% - 60px);
  overflow: hidden;
`;

