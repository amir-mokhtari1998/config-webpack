import React from 'react';
import styled from 'styled-components';

import ChatIcon from '@icons/ChatIcon';
import { FlexBox, Typography } from '@src/theme/index';

interface MyComponentProps {
  image: string;
  name: string;
  timeIsActive: string;
}

const InfoChat: React.FC<MyComponentProps> = (props) => {
  return (
    <InfoChatStyled alignItems='center' justify='space-around'>
      <ImageStyled src={props.image} />
      <div>
        <Typography size='md' weight='bold'>{props.name}</Typography>
        <div>
          <ChatIcon height='12px' width='12px' fill='#7749f3' />
          <Typography size='sm' margin='0 0 0 2px'>{props.timeIsActive}</Typography>
        </div>
      </div>
    </InfoChatStyled>
  );
};

export default InfoChat;

const InfoChatStyled = styled(FlexBox)`
  font-family: ${({ theme }) => theme.fonts.english};
`;

const ImageStyled = styled.img`
  width: 36px;
  margin-right: 10px;
`;
