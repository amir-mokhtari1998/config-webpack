import React from 'react';
import styled from 'styled-components';

import InfoChat from './InfoChat';
import BackIcon from '@icons/BackIcon';
import MenuIcon from '@icons/MenuIcon';
import Bot from '@assets/image/bot.png';
import { FlexBox } from '@src/theme/index';

const HeaderChat: React.FC = () => {
  return (
    <HeaderChatStyled alignItems='center' justify='space-around'>
      <FlexBox alignItems='center' justify='space-between'>
        <BackIcon height='16px' width='16px' fill='#080c25' style={{ marginRight: '18px' }} />
        <InfoChat image={Bot} name='support bot' timeIsActive='always online' />
      </FlexBox>
      <MenuIcon height='17px' width='17px' fill='#7749f3' />
    </HeaderChatStyled>
  );
};

export default HeaderChat;

const HeaderChatStyled = styled(FlexBox)`
  height: 60px;
  background-color: ${props => props.theme.colors.secondary};
  border-radius: 0px 0px 25px 25px;
`;
