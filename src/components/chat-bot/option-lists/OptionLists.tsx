import React, { useEffect, useRef, useState } from 'react';
import styled, { keyframes } from 'styled-components';

import Option from './Option';
import { Button } from '@src/theme';
import { ScenarioProps } from '@src/types/component/chat-bot';

interface MyComponentProps {
  isOpen?: boolean;
  options?: { [key: string]: any }
  item?: ScenarioProps,
  textBtn?: string;
  handleClickButton?: (item: ScenarioProps) => void,
  onChangeHeight?: React.Dispatch<React.SetStateAction<number>>
}

const OptionLists: React.FC<MyComponentProps> = (props) => {

  const [activeItem, onChangeActiveItem] = useState<ScenarioProps | null>(null);
  const ref = useRef<HTMLDivElement>();

  const handleGetItem = (option: ScenarioProps) => {
    onChangeActiveItem(option);
  };

  const handleSendItem = () => {
    props.handleClickButton(activeItem);
    onChangeActiveItem(null);
  };

  useEffect(() => {
    if (ref.current) {
      props.onChangeHeight(ref.current.clientHeight);
    }
    return () => props.onChangeHeight(0);
  }, [props.isOpen]);

  return (
    <>
      {(props.isOpen && props.options) &&
        <OptionListStyled ref={ref}>
          <div>
            {
              props.options.map((item: ScenarioProps) =>
                <Option key={item.id} text={item.title} isActive={activeItem?.id === item.id}
                        handleClick={handleGetItem} item={item} />
              )
            }
          </div>
          <Button margin='16px 0 0 0' bg='button' size='sm' block radius='sm'
                  onClick={handleSendItem} disabled={!activeItem} active={!!activeItem}>
            {props.textBtn}
          </Button>
        </OptionListStyled>
      }
    </>
  );
};

export default OptionLists;

const slideDown = keyframes`
  from {
    left: -100%;
  }
  to {
    left: 0;
  }
`;

const OptionListStyled = styled.div`
  position: relative;
  width: 100%;
  background-color: ${props => props.theme.colors.secondary};
  box-shadow: rgb(0 0 0 / 12%) 0px 2px 24px;
  border-radius: 25px 25px 0px 0px;
  padding: 16px;
  display: grid;
  align-items: center;
  animation: ${slideDown} .8s forwards;
`;
