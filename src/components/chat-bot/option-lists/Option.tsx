import styled from 'styled-components';
import React from 'react';

import SendIcon from '@icons/SendIcon';
import { FlexBox, Typography } from '@src/theme';
import { ScenarioProps } from '@src/types/component/chat-bot';

interface MyComponentProps {
  text?: string;
  isActive?: boolean;
  handleClick?: (option: ScenarioProps) => void;
  item?: ScenarioProps
}

const Option: React.FC<MyComponentProps> = (props) => {
  return (
    <OptionStyled alignItems='center' justify='space-between'
                  isActive={props.isActive} onClick={() => props.handleClick(props.item)}>
      <Typography size='sm' align='justify' width='80%'> {props.text}</Typography>
      <SendIcon height='20px' width='20px' fill={props.isActive ? '#ffffff' : '#7749f3'} />
    </OptionStyled>
  );
};

export default Option;

const OptionStyled = styled(FlexBox)<{ isActive: boolean }>`
  cursor: pointer;
  width: 100%;
  margin: 8px 0;
  padding: 10px;
  border-radius: ${props => props.theme.borderRadius.sm};
  border: 1px solid ${props => props.theme.colors.primary};
  direction: rtl;
  ${(props) => (props.isActive ?
                  `background-color: ${props.theme.colors.primary};
             color: ${props.theme.colors.secondary};` :
                  `background-color: transparent;
             color: ${props.theme.colors.primary};
            `
  )};
`;
