import React from 'react';
import styled, { keyframes } from 'styled-components';

import { FlexBox } from '@src/theme';

type TypeMessage = {
  type?: string
}

const Loading: React.FC<TypeMessage> = (props) =>
  <FlexBox style={{ height: '40px' }} justify={props.type === 'bot' ? 'start' : 'end'} alignItems='center'>
    <DotPulse animated />
    <DotPulse />
    <DotPulse />
  </FlexBox>;


export default Loading;

const animationLoading = keyframes`
  0%, 80%, 100% {
    transform: scale(0);
  }
  40% {
    transform: scale(1.0);
  }
`;

const DotPulse = styled.div<{ animated?: boolean }>`
  width: 4px;
  height: 4px;
  margin: 0 2px;
  background-color: ${props => props.theme.colors.primary};
  border-radius: ${props => props.theme.borderRadius.md};
  animation: ${animationLoading} 1.4s infinite ease-in-out both;
  ${(props) => (props.animated ? `animation-delay: -0.32s;` : `animation-delay: -0.16s;`)};
`;
