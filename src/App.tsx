import React from 'react';
import { hot } from 'react-hot-loader';
import { ThemeProvider, DefaultTheme } from 'styled-components';

import { GlobalStyle } from '@styles/global-style';
import Main from '@components/chat-bot/Main';

interface Props {
  theme: DefaultTheme;
}

const App: React.FC<Props> = ({ theme }) =>

  <ThemeProvider theme={theme}>
    <Main />
    <GlobalStyle />
  </ThemeProvider>

export default hot(module)(App);
