import React, { useEffect, useRef } from 'react';

import { delay } from '@src/helper/delay';

export function UseChatScroll<T, T1>(message?: T, height?: T1): React.MutableRefObject<HTMLDivElement> {

  const ref = useRef<HTMLDivElement>();

  useEffect(() => {
    if (ref.current && height) {
      delay(500).then(() => ref.current.scrollIntoView({ behavior: 'smooth' }));
    } else {
      ref.current.scrollIntoView({ behavior: 'smooth' });
    }
  }, [message, height]);

  return ref;

}
