import { ColorProps, FontsProps, FontWeightsProps, FontSizeProps, BorderRadiusProps } from '@src/types/typograpy';

export const colors: ColorProps = {
  primary: '#7749f3',
  secondary: '#ffffff',
  button: '#00d170',
  text: '#080c25',
  default: '#000000',
  inherit: 'inherit'
};

export const fontFamily: FontsProps = { english: 'varela' , persian : 'iranSans' };

export const weight: FontWeightsProps = {
  bold: 700,
  regular: 500,
  normal: 400
};

export const fontSize: FontSizeProps = {
  sm: '12px',
  md: '14px',
  lg: '16px'
};

export const borderRadius: BorderRadiusProps = {
  sm: '8px',
  md: '20px',
  none: 'none'
};
