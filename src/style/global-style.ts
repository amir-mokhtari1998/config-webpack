import { createGlobalStyle } from 'styled-components';

import english from '@assets/fonts/varela/woff/VarelaRound-Regular.woff';
import persian from '@assets/fonts/iranSans/woff/IRANSansXFaNum-Medium.woff';

export const GlobalStyle = createGlobalStyle`
  @font-face {
    font-family: 'varela';
    font-style: normal;
    font-weight: normal;
    src: url(${english}) format('woff')
  }

  @font-face {
    font-family: 'iranSans';
    font-style: normal;
    font-weight: normal;
    src: url(${persian}) format('woff')
  }

  *,
  body {
    padding: 0;
    margin: 0;
    box-sizing: border-box;
    font-family: ${({ theme }) => theme.fonts.persian};
  }
`;
