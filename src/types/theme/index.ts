import { DefaultTheme } from 'styled-components';

import { weight, colors, fontSize, fontFamily, borderRadius } from '@src/style/variables';

export const defaultTheme: DefaultTheme = {
  fonts: fontFamily,
  fontSize,
  colors,
  fontWeight: weight,
  borderRadius
};
