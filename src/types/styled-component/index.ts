import { FontsProps, ColorProps, FontWeightsProps, FontSizeProps, BorderRadiusProps } from '@src/types/typograpy';

export interface ThemeType {
  fonts: FontsProps;
  fontWeight: FontWeightsProps;
  colors: ColorProps;
  fontSize: FontSizeProps;
  borderRadius: BorderRadiusProps;
}
