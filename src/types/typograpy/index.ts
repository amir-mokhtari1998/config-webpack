export interface FontsProps {
  english: string;
  persian: string;
}

export type FontKeys = keyof FontsProps

export interface ColorProps {
  primary: string;
  secondary: string;
  button: string;
  text: string;
  default: string;
  inherit: string;
}

export type ColorKeys = keyof ColorProps

export interface FontWeightsProps {
  bold: string | number;
  regular: string | number;
  normal: string | number;
}

export type WeightKeys = keyof FontWeightsProps

export interface FontSizeProps {
  sm: string | number;
  md: string | number;
  lg: string | number;
}

export type SizeKeys = keyof FontSizeProps

export interface BorderRadiusProps {
  sm: string | number;
  md: string | number;
  none: string | number;
}

export type BorderRadiusKeys = keyof BorderRadiusProps
