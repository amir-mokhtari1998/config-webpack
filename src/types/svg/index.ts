import React from 'react';

export type SvgIcon<P = unknown> = React.FunctionComponent<React.SVGProps<SVGSVGElement> & { title?: string } & P>

export interface SvgProps {
  height?: string,
  width?: string,
  fill?: string,
  style?: React.CSSProperties
}
