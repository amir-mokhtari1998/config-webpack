export interface ScenarioProps {
  id?: number
  title?: string
  options?: { [key: string]: any }
  responses?: any
  type?: string
  isLoading?: boolean
}
