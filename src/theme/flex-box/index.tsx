import styled from 'styled-components';

import { FlexProps } from '@src/types/flex-box';

interface MyPropsComponent extends FlexProps {
  inline?: boolean;
}

export const FlexBox = styled.div<MyPropsComponent>`
  display: ${({ inline }) => (inline ? 'inline-flex' : 'flex')};
  justify-content: ${({ justify }) => justify};
  align-content: ${({ alignContent }) => alignContent};
  align-items: ${({ alignItems }) => alignItems};
  flex-wrap: ${({ wrap }) => wrap};
  flex-direction: ${({ direction }) => direction};
  flex: ${({ flex }) => flex};
`;

FlexBox.defaultProps = {
  justify: 'initial',
  alignContent: 'initial',
  alignItems: 'initial',
  direction: 'row',
  wrap: 'nowrap',
  flex: 'unset'
};
