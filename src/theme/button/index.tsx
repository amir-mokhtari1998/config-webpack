import styled, { css } from 'styled-components';

import { BorderRadiusKeys, ColorKeys } from '@src/types/typograpy';
import { defaultTheme } from '@src/types/theme';

type ButtonSizes = 'sm' | 'md' | 'lg'
type ButtonAlign = 'center' | 'left' | 'right';
type ButtonBorder = 'none' | '1px' | '2px' | '3px' | '4px' | '5px'

interface ButtonProps {
  size?: ButtonSizes;
  align?: ButtonAlign;
  color?: ColorKeys;
  margin?: string;
  bg?: ColorKeys;
  borderColor?: ColorKeys;
  radius?: BorderRadiusKeys;
  block?: boolean;
  round?: boolean;
  textShadow?: boolean;
  border?: ButtonBorder;
  outLine?: string;
  cursor?: string;
  active?: boolean;
}

const size = ({ size, block, textShadow }: ButtonProps) => {

  let width = block ? '100%' : 'auto';
  let height = '';

  switch (size) {
    case 'sm':
      width = width;
      height = '38px';
      break;
    case 'md':
      width = width;
      height = '48px';
      break;
    default:
      break;
  }

  return css`
    width: ${width};
    height: ${height};
    ${textShadow ? 'text-shadow: 0px 1px 0px rgba(0, 0, 0, 0.24);' : ''}
  `;
};

export const Button = styled.button<ButtonProps>`
  ${({
       theme = defaultTheme, align, color, margin, bg, radius, round, border, outLine, borderColor, cursor, active
     }) => css`
    text-align: ${align};
    background-color: ${theme.colors[bg]};
    color: ${theme.colors[color]};
    margin: ${margin};
    ${size};
    ${border !== 'none' ? `border: ${border} solid ${theme.colors[borderColor]}` : 'border: none;'};
    border-radius: ${round ? '50%' : theme.borderRadius[radius]};
    outline: ${outLine};
    cursor: ${cursor};
    ${active ? `opacity: 1;` : 'opacity: 0.5;'};
  `
  };
`;

Button.defaultProps = {
  align: 'center',
  round: false,
  size: 'sm',
  color: 'secondary',
  outLine: 'none',
  border: 'none',
  cursor: 'pointer'
};
