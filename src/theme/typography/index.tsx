import styled, { css } from 'styled-components';

import { ColorKeys, FontKeys, SizeKeys, WeightKeys } from '@src/types/typograpy';
import { defaultTheme } from '@src/types/theme';

export interface TextProps {
  family?: FontKeys;
  weight?: WeightKeys,
  size?: SizeKeys,
  color?: ColorKeys;
  margin?: string;
  padding?: string;
  inline?: boolean;
  width?: string;
  maxWidth?: string;
  align?:
    | 'center'
    | 'justify'
    | 'left'
    | 'start'
    | 'right'
    | 'end'
    | 'initial'
    | 'revert'
    | 'unset';
  direction?: 'rtl' | 'ltr'
}

export const Typography = styled.p<TextProps>`
  ${({
       theme = defaultTheme,
       family, size, weight, margin, padding, inline, align, color, width, maxWidth, direction
     }) =>
          css`
            font-family: ${theme.fonts[family]};
            font-size: ${theme.fontSize[size]};
            font-weight: ${theme.fontWeight[weight]};
            color: ${theme.colors[color]};
            margin: ${margin};
            padding: ${padding};
            display: ${inline ? 'inline' : 'inline-block'};
            text-align: ${align};
            width: ${width};
            max-width: ${maxWidth};
            direction: ${direction};
          `
  };
`;

Typography.defaultProps = {
  family: 'persian',
  size: 'sm',
  weight: 'normal',
  color: 'inherit',
  inline: false,
  align: 'center',
};
